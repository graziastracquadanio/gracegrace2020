Import the file `'abstracts/media-queries'` in your component and use the mixins `@mixin media-breakpoint-up($bp)` or `@mixin media-breakpoint-down($bp)`.

```scss
@import 'abstracts/media-queries';

.class-name {
  // whatever default style
}

@include media-breakpoint-down($breakpoint-laptop) {
  .class-name {
    // whatever needs to be adapted
    // before that breakpoint
  }
}

@include media-breakpoint-up($breakpoint-laptop) {
  .class-name {
    // whatever needs to be adapted
    // after that breakpoint
  }
}
```
