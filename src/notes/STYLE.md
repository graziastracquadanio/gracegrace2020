#### Tools

We write CSS using [Sass](https://sass-lang.com/guide) preprocessor using the [Scss](http://sass-lang.com/documentation/file.SCSS_FOR_SASS_USERS.html) syntax.

Before adding any line of cool style, you may want to have a quick look at [this Sass guidelines](https://sass-guidelin.es/#syntax--formatting).

With particular focus on this paragraphs:

- [Syntax & formatting](https://sass-guidelin.es/#syntax--formatting)
- [Naming conventions](https://sass-guidelin.es/#naming-conventions)
- [Component structure](https://sass-guidelin.es/#component-structure)

#### Components

> Use the `.component-descendant-descendant` pattern for components.

Components help encapsulate your CSS and prevent run-away cascading styles and keep things readable and maintainable. Central to componentizing CSS is namespacing. Instead of using descendant selectors, like `.header img { … }`, you’ll create a new hyphen-separated class for the descendant element, like `.header-image { … }`.

Here’s an example with descendant selectors:

```SCSS
.global-header {
  background: hsl(202, 70%, 90%);
  color: hsl(202, 0%, 100%);
  height: 40px;
  padding: 10px;
}

.global-header .logo {
  float: left;
}

.global-header .logo img {
  height: 40px;
  width: 200px;
}

.global-header .nav {
  float: right;
}

.global-header .nav .item {
  background: hsl(0, 0%, 90%);
  border-radius: 3px;
  display: block;
  float: left;
  -webkit-transition: background 100ms;
  transition: background 100ms;
}

.global-header .nav .item:hover {
  background: hsl(0, 0%, 80%);
}
```

And here’s the same example with namespacing:

```SCSS
.global-header {
  background: hsl(202, 70%, 90%);
  color: hsl(202, 0%, 100%);
  height: 40px;
  padding: 10px;
}

  .global-header-logo {
    float: left;
  }

    .global-header-logo-image {
      background: url("logo.png");
      height: 40px;
      width: 200px;
    }

  .global-header-nav {
    float: right;
  }

    .global-header-nav-item {
      background: hsl(0, 0%, 90%);
      border-radius: 3px;
      display: block;
      float: left;
      -webkit-transition: background 100ms;
      transition: background 100ms;
    }

    .global-header-nav-item:hover {
      background: hsl(0, 0%, 80%);
    }
```

Namespacing keeps specificity low, which leads to fewer inline styles, !important declarations, and makes things more maintainable over time.

Make sure **every selector is a class**. There should be no reason to use id or element selectors. No underscores or camelCase. Everything should be lowercase.

Components make it easy to see relationships between classes. You just need to look at the name. You should still **indent descendant classes** so their relationship is even more obvious and it is easier to scan the file. Stateful things like `:hover` should be on the same level.

#### Modifiers

> Use the `.component-descendant.mod-modifier` pattern for modifier classes.

Let’s say you want to use a component, but style it in a special way. We run into a problem with namespacing because the class needs to be a sibling, not a child. Naming the selector `.component-descendant-modifier` means the modifier could be easily confused for a descendant. To denote that a class is a modifier, use a `.mod-modifier` class.

For example, we want to specially style our sign up button among the header buttons. We’ll add `.global-header-nav-item.mod-sign-up`, which looks like this:

```HTML
<!-- HTML -->

<a class="global-header-nav-item mod-sign-up">
  Sign Up
</a>
```

```SCSS
// global-header.scss

.global-header-nav-item {
  background: hsl(0, 0%, 90%);
  border-radius: 3px;
  display: block;
  float: left;
  -webkit-transition: background 100ms;
  transition: background 100ms;
}

.global-header-nav-item.mod-sign-up {
  background: hsl(120, 70%, 40%);
  color: #fff;
}
```

We inherit all the `global-header-nav-item` styles and modify it with `.mod-sign-up`. This breaks our namespace convention and increases the specificity, but that’s exactly what we want. This means we don’t have to worry about the order in the file. For the sake of clarity, put it after the part of the component it modifies. Put modifiers on the same indention level as the selector it is modifying.

**You should never write a bare `.mod-` class**. It should always be tied to a part of a component. `.header-button.mod-sign-up { background: green; }` is good, but `.mod-sign-up { background: green; }` is bad. We could be using `.mod-sign-up` in another component and we wouldn’t want to override it.

You’ll often want to overwrite a descendant of the modified selector. Do that like so:

```SCSS
.global-header-nav-item.mod-sign-up {
  background: hsl(120, 70%, 40%);
  color: #fff;

  .global-header-nav-item-text {
    font-weight: bold;
  }

}
```

Generally, we try and avoid nesting because it results in runaway rules that are impossible to read. This is an exception.

Put modifiers at the bottom of the component file, after the original components.

#### State

> Use the `.component-descendant.is-state` pattern for state. Manipulate `.is-` classes in JavaScript (but not presentation classes).

State classes show that something is enabled, expanded, hidden, etc. For these classes, we’ll use a new `.component-descendant.is-state` pattern.

Example: Let’s say that when you click the logo, it goes back to your home page. But because it is a single page app, it needs to load things. You want your logo to do a loading animation. This should sound familiar to Trello users.

You’ll use a `.global-header-logo-image.is-loading` rule. That looks like this:

```SCSS
.global-header-logo-image {
  background: url('logo.png');
  height: 40px;
  width: 200px;
}

.global-header-logo-image.is-loading {
  background: url('logo-loading.gif');
}
```

JavaScript defines the state of the application, so we’ll use JavaScript to toggle the state classes. The `.component.is-state` pattern decouples state and presentation concerns so we can add state classes without needing to know about the presentation class. A developer can just say to the designer, “This element has an .is-loading class. You can style it however you want.”. If the state class were something like `global-header-logo-image--is-loading`, the developer would have to know a lot about the presentation and it would be harder to update in the future.

Like modifiers, it is possible that the same state class will be used on different components. You don’t want to override or inherit styles, so it is important that **every component define its own styles for the state**. They should never be defined on their own. Meaning you should see `.global-header.is-hidden { display: none; }`, but never `.is-hidden { display: none; }` (as tempting as that may be). `.is-hidden` could conceivably mean different things in different components.

We also don’t indent state classes. Again, that’s only for descendants. State classes should appear at the bottom of the file, after the original components and modifiers.

#### Keeping It Encapsulated

Components can control a large part of the layout or just a button. In your templates, you’ll likely end up with parts of one component inside another component, like a `.button` inside a `.member-list`. We need to change the button’s size and positioning to fit the list.

This is tricky. Components shouldn’t know anything about each other. If the smaller button can be reused in multiple places, add a modifier in the button component (like, `.button.mod-small`) and use it in member-list. Do the positioning with a member list component with a descendant, since that’s specific to the member list and not the button.

Here’s an example:

```HTML
<!-- HTML -->

<div class="member-list">
  <div class="member-list-item">
    <p class="member-list-item-name">Gumby</p>
    <div class="member-list-item-action">
      <a href="#" class="button mod-small">Add</a>
    </div>
  </div>
</div>
```

```SCSS
// button.scss

.button {
  background: #fff;
  border: 1ps solid #999;
  padding: 8px 12px;
}

.button.mod-small {
  padding: 6px 10px;
}


// member-list.scss

.member-list {
  padding: 20px;
}

  .member-list-item {
    margin: 10px 0;
  }

    .member-list-item-name {
      font-weight: bold;
      margin: 0;
    }

    .member-list-item-action {
      float: right;
    }
```

A _bad_ thing to do would be this:

```HTML
<!-- HTML -->

<div class="member-list">
  <div class="member-list-item">
    <p class="member-list-item-name">Pat</p>
    <a href="#" class="member-list-item-button button">Add</a>
  </div>
</div>
```

```SCSS
// member-list.scss

.member-list-item-button {
  float: right;
  padding: 6px 10px;
}
```

In the _bad_ example, `.member-list-item-button` overrides styles specific to the button component. It assumes things about button that it shouldn’t have to know anything about. It also prevents us from reusing the small button style and makes it hard to clean or change up later if needed.

You should end up with a lot of components. Always be asking yourself if everything inside a component is absolutely related and can’t be broken down into more components. If you start to have a lot of modifiers and descendants, it might be time to break it up.

#### Code style

Even following the above guidelines, it is still possible to write CSS in a ton of different ways. Writing our CSS in a consistent way makes it more readable for everyone. Take this bit of CSS:

```SCSS
.global-header-nav-item {
  background: hsl(0, 0%, 90%);
  border-radius: 3px;
  display: block;
  float: left;
  padding: 8px 12px;
  -webkit-transition: background 100ms;
  transition: background 100ms;
}
```

It sticks to these style rules:

- Use a new line for every selector and every declaration.
- Use two new lines between rules.
- Add a single space between the property and value, for example `prop: value;` and not `prop:value;`.
- Alphabetize declarations.
- Use 2 spaces to indent, not 4 spaces and not tabs.
- No underscores or camelCase for selectors.
- Use shorthand when appropriate, like `padding: 15px 0;` and not `padding: 15px 0px 15px 0px;`.
