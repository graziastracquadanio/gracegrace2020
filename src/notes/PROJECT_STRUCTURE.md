#### App structure

It is very important to apply the [Single responsibility principle](https://en.wikipedia.org/wiki/Single_responsibility_principle) so that we can break down the application in dedicated modules, components, classes, etc...

This is the file system that will be adopted (v.1). It could change in the future based on the application's needs.

```shell
.
|-- app.module.ts
|-- app.component.ts
|-- module1/
|   |-- common/
|   |-- pages/
|   |-- services/
|   |-- module1-routing.module.ts
|   |-- module1.module.ts
|
|-- module2/
|   |-- common/
|   |-- pages/
|   |-- services/
|   |-- module1-routing.module.ts
|   |-- module1.module.ts
|
|-- common/
    |-- components/
    |-- services/
    |-- mocks/
    |-- models/
    ...
```

When create a module? Basically we will create a dedicated module for a part of the application that implements one feature or few features deeply connected. For example, Backoffice will be a module, with few pages inside.

#### Global styles structure

The common styles are in the fwk-ui library. All the other styles are related to the single components.

**Avoid using `ViewEncapsulation.None` in the components, unless is very specific case related to a single app.**

In the folder `styles` will go anything related to the style (only `scss`) that is common across the components of project.

```shell
.
|-- styles/
    |-- abstract/
    |   |-- _variables.scss
    |   |-- _mixins.scss
    |   |-- _media-queries.scss
    |   ...
    |
    |-- base/
    |   |-- _reset.scss
    |   |-- _typography.scss
    |   ...
    |
    |-- components/   // see if we actually need that
    |-- layout/
    |-- vendor/       // if needed
    |-- main.scss     // main entry point
    |
```

The main file is `main.scss` and this file should not contain anything but @import and comments.

###### Abstracts

The `abstracts/` folder gathers all Sass tools and helpers used across the project. Every global variable, function, mixin and stuff should be put in here.

The rule of thumb for this folder is that it should not output a single line of CSS when compiled on its own. These are nothing but Sass helpers.

[Reference](http://sass-guidelin.es/#abstracts-folder)

###### Base

The `base` folder includes all the rules that define how the markup would look without classes applied to it.

Base styles include setting heading sizes, default link styles, default font styles, and body backgrounds. There should be no need to use !important in a Base style.

[Reference](https://smacss.com/book/type-base)

###### Layout

The `layout/` folder contains everything that takes part in laying out the site or application. This folder will have stylesheets for the main parts of the site (header, footer, navigation, sidebar…), or the grid system.

[Reference](https://smacss.com/book/type-layout)

###### Vendors

The `vendors/` folder contains all styles from external libraries and frameworks.

Read this... (https://sass-guidelin.es/#shame-file)
