As a team is very important that all the team members write code following the same rules.
Why? Because it is much easier to understand and maintain a large codebase when the code has a consistent style.

If you are modifying this project you may be pointed to one of the following links to see the style guides applied.

The [Google HTML/CSS style guide](https://google.github.io/styleguide/htmlcssguide.html) and
[JavaScript style guide](https://google.github.io/styleguide/jsguide.html) are the basics for our coding style,
with additional guidance here where that style guide is not aligned with ES6 or Typescript.

###### More scss style code

In addition, we follow the [Angular style guide and best practices](https://angular.io/guide/styleguide).

The stylesheets applied to this project are written with [Sass](https://sass-lang.com/guide).

Before adding any line of cool style, you may want to have a quick look at [this Sass guidelines](https://sass-guidelin.es/#syntax--formatting).

- [Syntax & formatting](https://sass-guidelin.es/#syntax--formatting)
- [Naming conventions](https://sass-guidelin.es/#naming-conventions)
- [Component structure](https://sass-guidelin.es/#component-structure)
