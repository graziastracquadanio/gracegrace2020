import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CookbookModule } from './cookbook/cookbook.module';
import { OtherStuffModule } from './other-stuff/other-stuff.module';
import { StyleguideModule } from './styleguide/styleguide.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // App modules
    CookbookModule,
    OtherStuffModule,
    StyleguideModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
