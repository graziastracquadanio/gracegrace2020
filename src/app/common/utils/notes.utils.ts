export const getNoteSrc = (name: string): string => {
  const page = name.toUpperCase().replace(/-/g, '_');
  return `notes/${page}.md`;
};
