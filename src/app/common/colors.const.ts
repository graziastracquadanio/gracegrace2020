// NOTE: If this file changes remember to update also
// this file '_colors.scss'

export const Colors = {
  // colorsPalette
  colorPrimaryDark: '#933969',
  colorPrimary: '#bf538d',
  colorPrimaryLight: '#e3bcd6',
  colorSecondaryDark: '#689f38',
  colorSecondary: '#9ccc65',
  colorSecondaryLight: '#dcedc8',
  colorAccentDark: '#ff8e01',
  colorAccent: '#ffca28',
  colorAccentLight: '#ffecb3',
  colorGrayDark: '#5a5c65',
  colorGray: '#868993',
  colorGrayLight: '#c9c9c9',
  colorBackgroundDark: '#dbdbdb',
  colorBackground: '#f4f4f4',
  colorBackgroundLight: '#ffffff',

  // alertColors
  colorSuccess: '#37a037',
  colorInfo: '#319cdd',
  colorDanger: '#c62c2c',
  colorSuccessLight: '#d7f4d7',
  colorInfoLight: '#cceaf2',
  colorDangerLight: '#eac7c7',
};
