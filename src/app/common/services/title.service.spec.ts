import { TestBed } from '@angular/core/testing';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';

import { from } from 'rxjs';

import { TitleService } from './title.service';

describe('TitleService', () => {
  let service: TitleService;
  let title, router, activatedRoute;

  const navigationEnd = new NavigationEnd(1, 'navigationEndUrl', 'navigationEndUrl');
  const navigationStart = new NavigationStart(2, 'navigationStartUrl');
  const events = [navigationStart, navigationEnd];

  beforeEach(() => {
    title = jasmine.createSpyObj('Title', ['setTitle']);

    router = jasmine.createSpyObj('Router', ['events']);
    router.events = from(events);

    activatedRoute = {
      firstChild: {
        data: { value: { title: 'Test section' } },
        firstChild: { data: { value: { title: 'Test title' } } },
      },
    };

    TestBed.configureTestingModule({
      providers: [
        { provide: Title, useValue: title },
        { provide: Router, useValue: router },
        { provide: ActivatedRoute, useValue: activatedRoute },
        TitleService,
      ],
    });
    service = TestBed.get(TitleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update title on navigationEnd event', () => {
    service.setTitlesOnNavigation();

    expect(title.setTitle).toHaveBeenCalledTimes(1);
    expect(title.setTitle).toHaveBeenCalledWith('Test title | Test section');
  });
});
