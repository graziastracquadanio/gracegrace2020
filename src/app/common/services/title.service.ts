import { Injectable, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TitleService implements OnDestroy {
  constructor(private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute) {}

  private subscription;

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  setTitle(pageTitle: string): void {
    this.titleService.setTitle('GraceGrace - ' + pageTitle);
  }

  setTitlesOnNavigation(): void {
    this.subscription = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          const titlePath = [];
          while (route.firstChild) {
            route = route.firstChild;
            const title = route.data['value']['title'];
            if (title) {
              titlePath.push(title);
            }
          }
          return titlePath;
        })
      )
      .subscribe(titlePath => {
        if (titlePath.length > 0) {
          this.setTitle(titlePath.reverse().join(' | '));
        }
      });
  }
}
