import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[ggCodeHighlighter]',
})
export class CodeHighlighterDirective implements AfterViewInit {
  constructor(public el: ElementRef) {}

  ngAfterViewInit() {
    if (window['Prism']) {
      window['Prism'].highlightElement(this.el.nativeElement);
    }
  }
}
