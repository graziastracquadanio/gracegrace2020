import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[ggCopyToClipboard]',
})
export class CopyToClipboardDirective {
  @Input('ggCopyToClipboard') textToCopy: string;

  constructor(private el: ElementRef) {
    el.nativeElement.style.cursor = 'pointer';
    el.nativeElement.setAttribute('title', 'Copy to clipboard');
    el.nativeElement.classList.add('copy-to-clipboard-item');
  }

  @HostListener('click')
  onclick() {
    this.copyToClipboard();
  }

  private copyToClipboard() {
    let { textToCopy } = this;
    if (!textToCopy) {
      textToCopy = this.el.nativeElement.innerText;
    }

    this.copyTextToClipboard(textToCopy);
  }

  private copyTextToClipboard(text) {
    const txtArea = document.createElement('textarea');
    txtArea.style.opacity = '0';
    txtArea.value = text;
    document.body.appendChild(txtArea);
    txtArea.select();

    try {
      const successful = document.execCommand('copy');
      if (successful) {
        return true;
      }
    } catch (err) {
      console.log('Oops, unable to copy');
    } finally {
      document.body.removeChild(txtArea);
    }
    return false;
  }
}
