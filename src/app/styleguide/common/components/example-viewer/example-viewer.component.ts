import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'gg-example-viewer',
  templateUrl: './example-viewer.component.html',
  styleUrls: ['./example-viewer.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ExampleViewerComponent {
  sourceCode: object;
  showSourceCode: boolean;
  exampleTitle: string;
  fragment: string;

  @Input() componentName: string;
  @Input('exampleTitle') set _exampleTitle(val: string) {
    this.exampleTitle = val;
    this.fragment = this.exampleTitle.toLowerCase().replace(/\s/g, '-');
  }

  constructor() {}

  async toggleSourceCode() {
    if (!this.sourceCode) {
      // TODO
      // load source code
    }
    this.showSourceCode = !this.showSourceCode;
  }
}
