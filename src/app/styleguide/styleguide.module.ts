import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCode, faDesktop, faLaptop, faMobileAlt, faTabletAlt, faTv } from '@fortawesome/free-solid-svg-icons';
import { MarkdownModule } from 'ngx-markdown';

import { ExampleViewerComponent } from './common/components/example-viewer/example-viewer.component';
import { CodeHighlighterDirective } from './common/directives/code-highlighter.directive';
import { CopyToClipboardDirective } from './common/directives/copy-to-clipboard.directive';
import { NotesPageComponent } from './sections/notes-section/notes-page/notes-page.component';
import { ColorPageComponent } from './sections/visual-style-section/color-page/color-page.component';
import { MediaQueriesPageComponent } from './sections/visual-style-section/media-queries-page/media-queries-page.component';
import { TypographyPageComponent } from './sections/visual-style-section/typography-page/typography-page.component';
import { StyleguideRoutingModule } from './styleguide-routing.module';
import { StyleguideComponent } from './styleguide.component';

@NgModule({
  declarations: [
    // directives
    CodeHighlighterDirective,
    CopyToClipboardDirective,
    // components
    ColorPageComponent,
    ExampleViewerComponent,
    MediaQueriesPageComponent,
    StyleguideComponent,
    TypographyPageComponent,
    NotesPageComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    MarkdownModule.forRoot({ loader: HttpClient }),
    StyleguideRoutingModule,
  ],
})
export class StyleguideModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCode, faDesktop, faLaptop, faMobileAlt, faTabletAlt, faTv);
  }
}
