import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotesPageComponent } from './sections/notes-section/notes-page/notes-page.component';
import { ColorPageComponent } from './sections/visual-style-section/color-page/color-page.component';
import { MediaQueriesPageComponent } from './sections/visual-style-section/media-queries-page/media-queries-page.component';
import { TypographyPageComponent } from './sections/visual-style-section/typography-page/typography-page.component';
import { StyleguideComponent } from './styleguide.component';

const defaultPath = '/styleguide/visual-style/colors';
const defaultNotesPath = '/styleguide/dev-notes/project-structure';

const routes: Routes = [
  {
    path: 'styleguide',
    component: StyleguideComponent,
    children: [
      {
        path: 'visual-style',
        data: { title: 'Styleguide' },
        children: [
          {
            path: 'colors',
            component: ColorPageComponent,
            data: { title: 'Colors' },
          },
          {
            path: 'typography',
            component: TypographyPageComponent,
            data: { title: 'Typography' },
          },
          {
            path: 'media-queries',
            component: MediaQueriesPageComponent,
            data: { title: 'Media queries' },
          },
          {
            path: '',
            redirectTo: defaultPath,
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'dev-notes',
        data: { title: 'Dev notes' },
        children: [
          {
            path: ':page',
            component: NotesPageComponent,
            data: { title: null },
          },
          {
            path: '',
            redirectTo: defaultNotesPath,
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '',
        redirectTo: defaultPath,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StyleguideRoutingModule {}
