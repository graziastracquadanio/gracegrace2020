import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'gg-styleguide',
  templateUrl: './styleguide.component.html',
  styleUrls: ['./styleguide.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class StyleguideComponent {
  navigation = [
    {
      label: 'Styleguide',
      value: 'visual-style',
      children: [
        { label: 'Colors', value: 'colors' },
        { label: 'Typography', value: 'typography' },
        { label: 'Media queries', value: 'media-queries' },
      ],
    },
    {
      label: 'Dev notes',
      value: 'dev-notes',
      children: [
        { label: 'Project structure', value: 'project-structure' },
        { label: 'Style guidelines', value: 'style' },
      ],
    },
  ];
}
