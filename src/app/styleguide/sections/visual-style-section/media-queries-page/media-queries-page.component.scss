@import 'abstracts/variables';
@import 'abstracts/media-queries';

$breakpoint-icon-size: 4rem;

.breakpoint-container {
  padding: 2rem;
  margin-top: 2rem;
}

.breakpoint-chart {
  display: flex;
  padding: 0 $breakpoint-icon-size/2;
}

.breakpoint-chart-item {
  width: 25%;
  height: 1.5rem;
  background-color: $color-background-dark;
  position: relative;

  &:after {
    content: '';
    position: absolute;
    right: -0.125rem;
    top: 0;
    width: 0.25rem;
    height: 100%;
    background-color: white;
    z-index: 20;
  }

  &:last-child:after {
    display: none;
  }

  span {
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(50%, -110%);
  }
}

.breakpoint-list {
  display: flex;
  justify-content: space-between;
}

.breakpoint-item {
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  width: $breakpoint-icon-size;
  margin-top: 3rem;
  color: $color-gray-dark;

  & > * {
    margin-bottom: 1rem;
  }

  @include media-breakpoint-down($breakpoint-tablet) {
    &:nth-child(odd) {
      .breakpoint-item-value {
        margin-bottom: 2.5rem;
      }
    }

    .breakpoint-item-value {
      margin-bottom: 0;
    }
  }
}

.mod-disabled {
  opacity: 0.2;
}

.breakpoint-item-icon {
  font-size: $breakpoint-icon-size - 1;
}
