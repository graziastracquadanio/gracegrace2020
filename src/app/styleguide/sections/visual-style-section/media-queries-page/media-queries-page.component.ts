import { Component } from '@angular/core';

import { getNoteSrc } from '@common/utils/notes.utils';

@Component({
  selector: 'gg-media-queries-page',
  templateUrl: './media-queries-page.component.html',
  styleUrls: ['./media-queries-page.component.scss'],
})
export class MediaQueriesPageComponent {
  mediaQueriesSrc = getNoteSrc('media-queries');
}
