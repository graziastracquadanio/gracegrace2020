import { Component } from '@angular/core';

import { Colors } from '@common/colors.const';

@Component({
  selector: 'gg-color-page',
  templateUrl: './color-page.component.html',
  styleUrls: ['./color-page.component.scss'],
})
export class ColorPageComponent {
  colorsDefinition = [
    {
      label: 'Colors palette',
      items: [
        { label: '$color-primary-dark', value: Colors.colorPrimaryDark },
        { label: '$color-primary', value: Colors.colorPrimary },
        { label: '$color-primary-light', value: Colors.colorPrimaryLight },
        { label: '$color-secondary-dark', value: Colors.colorSecondaryDark },
        { label: '$color-secondary', value: Colors.colorSecondary },
        { label: '$color-secondary-light', value: Colors.colorSecondaryLight },
        { label: '$color-accent-dark', value: Colors.colorAccentDark },
        { label: '$color-accent', value: Colors.colorAccent },
        { label: '$color-accent-light', value: Colors.colorAccentLight },
        { label: '$color-gray-dark', value: Colors.colorGrayDark },
        { label: '$color-gray', value: Colors.colorGray },
        { label: '$color-gray-light', value: Colors.colorGrayLight },
        { label: '$color-background-dark', value: Colors.colorBackgroundDark },
        { label: '$color-background', value: Colors.colorBackground },
        { label: '$color-background-light', value: Colors.colorBackgroundLight },
      ],
    },
    {
      label: 'Alert colors',
      items: [
        { label: '$color-success', value: Colors.colorSuccess },
        { label: '$color-info', value: Colors.colorInfo },
        { label: '$color-danger', value: Colors.colorDanger },
        { label: '$color-success-light', value: Colors.colorSuccessLight },
        { label: '$color-info-light', value: Colors.colorInfoLight },
        { label: '$color-danger-light', value: Colors.colorDangerLight },
      ],
    },
  ];
}
