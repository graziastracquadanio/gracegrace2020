import { Component } from '@angular/core';

@Component({
  selector: 'gg-typography-page',
  templateUrl: './typography-page.component.html',
  styleUrls: ['./typography-page.component.scss'],
})
export class TypographyPageComponent {
  alphabet = 'abcdefghiklmnopqrstvxyz';

  primaryFont = 'Zilla Slab';
  secondaryFont = 'Open Sans';
}
