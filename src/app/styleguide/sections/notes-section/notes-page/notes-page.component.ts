import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';

import { TitleService } from '@common/services/title.service';
import { getNoteSrc } from '@common/utils/notes.utils';

@Component({
  selector: 'gg-notes-page',
  templateUrl: './notes-page.component.html',
  styleUrls: ['./notes-page.component.scss'],
})
export class NotesPageComponent implements OnInit, OnDestroy {
  pageSrc: string;

  subscription: Subscription;

  constructor(private route: ActivatedRoute, private titleService: TitleService) {}

  ngOnInit() {
    this.subscription = this.route.paramMap.subscribe(params => {
      const page = params.get('page');
      this.pageSrc = getNoteSrc(page);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
