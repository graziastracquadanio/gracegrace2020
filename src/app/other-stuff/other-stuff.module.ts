import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtherStuffRoutingModule } from './other-stuff-routing.module';
import { AboutPageComponent } from './pages/about-page/about-page.component';


@NgModule({
  declarations: [AboutPageComponent],
  imports: [
    CommonModule,
    OtherStuffRoutingModule
  ]
})
export class OtherStuffModule { }
