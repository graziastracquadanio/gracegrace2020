import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { TitleService } from './common/services/title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  loading: boolean;

  private subscription: Subscription;

  constructor(private router: Router, private titleService: TitleService) {}

  ngOnInit() {
    this.titleService.setTitlesOnNavigation();

    this.subscription = this.router.events
      .pipe(filter(e => e instanceof NavigationStart || e instanceof NavigationEnd))
      .subscribe(e => (this.loading = !(e instanceof NavigationEnd)));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
