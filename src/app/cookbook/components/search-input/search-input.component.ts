import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'gg-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit, OnDestroy {
  searchText = '';
  searchTextUpdate = new Subject<any>();
  subscription: Subscription;

  @Input() placeholder = 'What are you looking for?';

  @Output() valueChanged = new EventEmitter<string>();

  ngOnInit() {
    this.subscription = this.searchTextUpdate.pipe(debounceTime(1000)).subscribe((event: any) => {
      this.changingValue(event);
    });
  }

  ngOnDestroy() {}

  clearValue(shouldEmit: boolean = false): void {
    this.searchText = '';
    if (shouldEmit) {
      this.valueChanged.emit(this.searchText);
    }
  }

  changingValue(event: any): void {
    this.searchText = event.target.value;
    this.valueChanged.emit(this.searchText);
  }

  keypress(event: any): void {
    if (this.searchText && event.keyCode === 27) {
      this.clearValue(true);
    }
  }
}
