import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faArrowLeft, faSearch, faTimes } from '@fortawesome/free-solid-svg-icons';

import { SearchInputComponent } from './components/search-input/search-input.component';
import { CookbookRoutingModule } from './cookbook-routing.module';
import { RecipeDetailPageComponent } from './pages/recipe-detail-page/recipe-detail-page.component';
import { RecipeListPageComponent } from './pages/recipe-list-page/recipe-list-page.component';
import { RecipeNotFoundPageComponent } from './pages/recipe-not-found-page/recipe-not-found-page.component';

@NgModule({
  declarations: [RecipeListPageComponent, RecipeDetailPageComponent, RecipeNotFoundPageComponent, SearchInputComponent],
  imports: [CommonModule, CookbookRoutingModule, FontAwesomeModule],
})
export class CookbookModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faArrowLeft, faSearch, faTimes);
  }
}
