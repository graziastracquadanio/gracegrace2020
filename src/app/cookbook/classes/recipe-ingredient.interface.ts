export interface RecipeIngredientGroup {
  ingredients: RecipeIngredient[];
  name: string;
}

export interface RecipeIngredient {
  id: number;
  name: string;
  amount?: string;
  unit?: string;
  notes?: string;
}
