import { RecipeIngredientGroup } from './recipe-ingredient.interface';
import { RecipeTag } from './recipe-tag.interface';

export interface Recipe {
  id: number;
  name: string;
  slug: string;
  imageUrl?: string;
  tags?: RecipeTag[];
  ingredients?: RecipeIngredientGroup[];
  instructions?: { instructions: { text: string }[]; name: string }[];
}
