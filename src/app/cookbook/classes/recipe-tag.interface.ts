export interface RecipeTag {
  id?: number;
  name?: string;
  slug?: string;
  count?: number;
}
