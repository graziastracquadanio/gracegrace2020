import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeNotFoundPageComponent } from './recipe-not-found-page.component';

describe('RecipeNotFoundPageComponent', () => {
  let component: RecipeNotFoundPageComponent;
  let fixture: ComponentFixture<RecipeNotFoundPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeNotFoundPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeNotFoundPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
