import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gg-recipe-not-found-page',
  templateUrl: './recipe-not-found-page.component.html',
  styleUrls: ['./recipe-not-found-page.component.scss']
})
export class RecipeNotFoundPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
