import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { of } from 'rxjs';

import { RecipeListPageComponent } from './recipe-list-page.component';

describe('RecipeListPageComponent', () => {
  let component: RecipeListPageComponent;
  let fixture: ComponentFixture<RecipeListPageComponent>;

  const fakeActivatedRoute = {
    data: of({
      recipes: [],
    }),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
      declarations: [RecipeListPageComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
