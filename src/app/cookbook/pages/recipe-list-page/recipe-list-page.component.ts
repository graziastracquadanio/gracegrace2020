import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';

import { Recipe } from '../../classes';

@Component({
  selector: 'gg-recipe-list-page',
  templateUrl: './recipe-list-page.component.html',
  styleUrls: ['./recipe-list-page.component.scss'],
})
export class RecipeListPageComponent implements OnInit, OnDestroy {
  recipeAsList: Recipe[];
  recipes: any;
  letters: string[];

  activeFilter: string;
  activeTags: number[];

  private subscription: Subscription;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.subscription = this.route.data.subscribe(data => {
      this.recipeAsList = data.recipes;
      this.updateTags(this.recipeAsList);
      this.updateRecipes(this.recipeAsList);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  clearFilter(refreshRecipes?: boolean): void {
    this.activeFilter = null;

    if (refreshRecipes) {
      this.updateRecipes(this.recipeAsList);
    }
  }

  clearAllTags(refreshRecipes?: boolean): void {
    this.activeTags = null;

    if (refreshRecipes) {
      this.updateRecipes(this.recipeAsList);
    }
  }

  searchRecipes(searchValue: string): void {
    if (this.activeTags) {
      this.clearAllTags();
    }

    if (searchValue) {
      const searchItems = searchValue.split(/,\s*/);
      const filteredRecipes = this.recipeAsList.filter(
        recipe =>
          recipe.name.includes(searchValue) ||
          recipe.ingredients.some(ingredientsSet => {
            return ingredientsSet.ingredients.some(ingredient => {
              return searchItems.some(item => {
                return ingredient.name.toLowerCase().includes(item.toLowerCase());
              });
            });
          })
      );
      this.updateRecipes(filteredRecipes);
    } else {
      this.clearFilter(true);
    }
  }

  filterRecipesByTag(tag: number): void {
    if (this.activeTags) {
      this.clearFilter();
    }
  }

  private updateRecipes(newRecipes: Recipe[]): void {
    this.recipes = {};

    newRecipes.forEach(recipe => {
      const firstLetter = recipe.name[0].toUpperCase();
      if (this.recipes[firstLetter]) {
        this.recipes[firstLetter].push(recipe);
      } else {
        this.recipes[firstLetter] = [recipe];
      }
    });

    this.letters = Object.keys(this.recipes);
  }

  private updateTags(newRecipes: Recipe[]): void {}
}
