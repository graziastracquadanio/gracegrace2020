import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';

import { TitleService } from '../../../common/services/title.service';
import { Recipe } from '../../classes';

@Component({
  selector: 'gg-recipe-detail-page',
  templateUrl: './recipe-detail-page.component.html',
  styleUrls: ['./recipe-detail-page.component.scss'],
})
export class RecipeDetailPageComponent implements OnInit, OnDestroy {
  recipe: Recipe;

  private subscription: Subscription;

  constructor(private route: ActivatedRoute, private titleService: TitleService) {}

  ngOnInit() {
    this.subscription = this.route.data.subscribe(data => {
      this.recipe = data.recipe;
      this.titleService.setTitle(this.recipe.name);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
