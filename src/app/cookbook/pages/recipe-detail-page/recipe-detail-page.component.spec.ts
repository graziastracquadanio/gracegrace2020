import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { of } from 'rxjs';

import { RecipeDetailPageComponent } from './recipe-detail-page.component';
import { TitleService } from '../../../common/services/title.service';

describe('RecipeDetailPageComponent', () => {
  let component: RecipeDetailPageComponent;
  let fixture: ComponentFixture<RecipeDetailPageComponent>;

  let titleServiceSpy: TitleService;

  const fakeActivatedRoute = {
    data: of({
      recipe: {
        name: 'This is kind of a beautiful recipe',
      },
    }),
  };

  beforeEach(async(() => {
    titleServiceSpy = jasmine.createSpyObj('TitleService', ['setTitle']);

    TestBed.configureTestingModule({
      providers: [
        { provide: TitleService, useValue: titleServiceSpy },
        { provide: ActivatedRoute, useValue: fakeActivatedRoute },
      ],
      declarations: [RecipeDetailPageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
