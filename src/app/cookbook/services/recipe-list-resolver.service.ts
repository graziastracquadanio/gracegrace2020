import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { Recipe, RecipeTag } from '../classes';
import { RecipeDataService } from './recipe-data.service';

interface ResolverResponse {
  recipes?: Recipe[];
  tags?: RecipeTag[];
}

@Injectable({
  providedIn: 'root',
})
export class RecipeListResolverService implements Resolve<ResolverResponse[]> {
  constructor(private recipeDataService: RecipeDataService) {}

  resolve(): Observable<any[]> | Observable<never> {
    return this.recipeDataService.getRecipesList().pipe(take(1));
  }
}
