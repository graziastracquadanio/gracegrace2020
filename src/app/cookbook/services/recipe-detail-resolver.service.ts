import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';

import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

import { Recipe } from '../classes';
import { RecipeDataService } from './recipe-data.service';

@Injectable({
  providedIn: 'root',
})
export class RecipeDetailResolverService implements Resolve<Recipe> {
  constructor(private recipeDataService: RecipeDataService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Recipe> | Observable<never> {
    const slug = route.paramMap.get('slug');

    return this.recipeDataService.getRecipeDetails(slug).pipe(
      take(1),
      mergeMap(result => {
        if (result) {
          return of(result);
        } else {
          this.router.navigate(['recipes', 'not-found']);
          return EMPTY;
        }
      })
    );
  }
}
