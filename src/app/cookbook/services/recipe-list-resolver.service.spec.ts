import { TestBed } from '@angular/core/testing';

import { RecipeListResolverService } from './recipe-list-resolver.service';

describe('RecipeListResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecipeListResolverService = TestBed.get(RecipeListResolverService);
    expect(service).toBeTruthy();
  });
});
