import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Recipe, RecipeTag } from '../classes';

@Injectable({
  providedIn: 'root',
})
export class RecipeDataService {
  private baseURL = 'https://gracegrace.me/wp_admin/wp-json/wp/v2/';

  constructor(private http: HttpClient) {}

  getRecipeDetails(slug: string): Observable<Recipe> {
    const url = this.baseURL + 'wprm_recipe';
    const params = new HttpParams().set('slug', 'wprm-' + slug);
    return this.http
      .get<Recipe>(url, { params })
      .pipe(map(result => this.parseRecipeDetails(result[0])));
  }

  getRecipesList(): Observable<Recipe[]> {
    const url = this.baseURL + 'wprm_recipe';
    const params = new HttpParams().set('order', 'asc').set('orderby', 'title').set('per_page', '100');
    return this.http
      .get<any[]>(url, { params })
      .pipe(map(result => this.parseRecipeList(result)));
  }

  getAllCourses(): Observable<any[]> {
    const url = this.baseURL + 'wprm_course';
    return this.http.get<any[]>(url);
  }

  getAllCuisines(): Observable<any[]> {
    const url = this.baseURL + 'wprm_cuisine';
    return this.http.get<any[]>(url);
  }

  private parseRecipeTags(rawRecipe): RecipeTag[] {
    const tags = [];
    if (rawRecipe.tags) {
      Object.keys(rawRecipe.tags).forEach(category => tags.push(...rawRecipe.tags[category]));
    }
    return tags;
  }

  private parseRecipeDetails(rawData: any): Recipe {
    const { recipe: rawRecipe } = rawData;

    return {
      id: rawRecipe.id,
      name: rawRecipe.name,
      slug: rawData.slug.replace('wprm-', ''),
      tags: this.parseRecipeTags(rawRecipe),
      ingredients: rawRecipe.ingredients,
      instructions: rawRecipe.instructions,
      imageUrl: rawRecipe.image_url,
    };
  }

  private parseRecipeList(rawData: any[]): Recipe[] {
    return rawData.map(({ recipe: rawRecipe, ...rest }: any) => {
      return {
        id: rawRecipe.id,
        name: rawRecipe.name,
        slug: rest.slug.replace('wprm-', ''),
        ingredients: rawRecipe.ingredients,
        imageUrl: rawRecipe.video_thumb_url,
        tags: this.parseRecipeTags(rawRecipe),
      };
    });
  }
}
