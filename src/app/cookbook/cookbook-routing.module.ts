import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecipeDetailPageComponent } from './pages/recipe-detail-page/recipe-detail-page.component';
import { RecipeListPageComponent } from './pages/recipe-list-page/recipe-list-page.component';
import { RecipeNotFoundPageComponent } from './pages/recipe-not-found-page/recipe-not-found-page.component';
import { RecipeDetailResolverService } from './services/recipe-detail-resolver.service';
import { RecipeListResolverService } from './services/recipe-list-resolver.service';

const routes: Routes = [
  {
    path: 'recipes',
    component: RecipeListPageComponent,
    data: { title: 'Recipes' },
    resolve: {
      recipes: RecipeListResolverService,
    },
    children: [
      {
        path: 'not-found',
        component: RecipeNotFoundPageComponent,
        data: { title: 'Recipe not found' },
      },
      {
        path: '',
        redirectTo: '/recipes',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'recipes/:slug',
    component: RecipeDetailPageComponent,
    data: { title: 'Recipes' },
    resolve: {
      recipe: RecipeDetailResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CookbookRoutingModule {}
